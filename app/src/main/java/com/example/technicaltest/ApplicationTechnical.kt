package com.example.technicaltest

import android.app.Application
import android.content.Context
import com.example.technicaltest.utils.createTables
import com.google.firebase.FirebaseApp

class ApplicationTechnical() : Application() {

    companion object {
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()

        // Creación de tablas
        createTables(this)

        // Contexto
        appContext = applicationContext

        //Inicializar FirebaseApp
        FirebaseApp.initializeApp(this)


    }

}