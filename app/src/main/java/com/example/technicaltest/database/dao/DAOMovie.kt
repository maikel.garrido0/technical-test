package com.example.technicaltest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.technicaltest.models.Movie

@Dao
interface DAOMovie {
    // TODO: 5/1/22 Método para agregar, modificar, o eliminar películas de ROOM

    // INSERTAR PELÍCULA
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: Movie)

    // ELIMINAR PELÍCULA
    @Query("DELETE FROM movie")
    suspend fun deleteMovie()

    // CANTIDAD DE PELÍCULAS
    @Query("SELECT COUNT(id) FROM movie")
    fun getMovieSize(): LiveData<Int?>?

    @Query("SELECT * FROM movie ORDER BY  id DESC")
    fun getMovieLiveData() : LiveData<List<Movie>?>


}