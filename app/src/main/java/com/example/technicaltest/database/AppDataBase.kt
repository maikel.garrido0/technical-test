package com.example.technicaltest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.technicaltest.database.dao.DAOMovie
import com.example.technicaltest.models.Movie

@Database(entities = [Movie::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun DAOMovie(): DAOMovie

    companion object {
        private var INSTANCE: AppDataBase? = null
        fun getDatabase(context: Context?): AppDataBase {
            INSTANCE = INSTANCE ?: Room.databaseBuilder(
                context!!.applicationContext,
                AppDataBase::class.java,
                "Technical_Test"
            ).build()
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        fun clearTables(context: Context) {
            getDatabase(context).clearAllTables()
        }
    }
}