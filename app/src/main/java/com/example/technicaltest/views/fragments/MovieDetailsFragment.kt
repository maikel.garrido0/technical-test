package com.example.technicaltest.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.technicaltest.dataSource.RepoImpl
import com.example.technicaltest.dataSource.VMFactory
import com.example.technicaltest.dataSource.dataSource
import com.example.technicaltest.database.AppDataBase
import com.example.technicaltest.databinding.FragmentMovieDetailsBinding
import com.example.technicaltest.viewmodel.MainViewModel

class MovieDetailsFragment : DialogFragment() {

    private var _binding: FragmentMovieDetailsBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    val viewModel by activityViewModels<MainViewModel> { VMFactory(RepoImpl(dataSource(AppDataBase.getDatabase(requireContext().applicationContext)))) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMovieDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getMovieSelected().observe(viewLifecycleOwner, Observer {
            val URL_IMAGE = "https://image.tmdb.org/t/p/w500/"
            binding.idTitleMovie.text = it.title
            binding.idDescriptionMovie.text = it.overview
            Glide.with(binding.idImage).load(URL_IMAGE+it.poster_path).into(binding.idImage)
        })
    }
}