package com.example.technicaltest.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.technicaltest.databinding.FragmentMapBinding
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.technicaltest.services.LocationService
import com.example.technicaltest.services.RetrofitClient
import com.example.technicaltest.utils.message

class MapFragment : Fragment() {

    private var _binding: FragmentMapBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun checkService() : Boolean {
        val activityManager = activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (activityManager != null) {
            for (service in activityManager.getRunningServices(Int.MAX_VALUE)) {
                when (LocationService::class.java.name) {
                    service.service.className -> {
                        if (service.foreground) { return true }
                    }
                    else -> { return false }
                }
            }
        }
        return false
    }

    private fun iniciarServicio() {
        if(!checkService()) {
            val intent = Intent(requireContext(), LocationService::class.java)
            intent.action = RetrofitClient.INICIAR_SERVICIO
            requireActivity().startService(intent)
            requireContext().message("Se inició el servicio...")
        }
    }

    private fun stopServicio() {
        if(checkService()) {
            val intent = Intent(requireContext(), LocationService::class.java)
            intent.action = RetrofitClient.DETENER_SERVICIO
            requireActivity().startService(intent)
            requireContext().message("Se detuvo el servicio...")
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                iniciarServicio()
            } else {
                requireContext().message("Permiso denegado...")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(
                requireActivity().applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        } else { iniciarServicio() }
    }

    override fun onPause() {
        super.onPause()
        stopServicio()
    }


}