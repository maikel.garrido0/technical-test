package com.example.technicaltest.views.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.technicaltest.databinding.ActivitySplashBinding
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {

    private var _binding: ActivitySplashBinding? = null
    private val binding get() = _binding!!

    val job = Job()
    val errorHandler = CoroutineExceptionHandler { coroutineContext, throwable ->  }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        splash()
    }

    // TODO: 5/1/22 SE INICIA EL SPLASH CON EL LOGO DE THE MOVIE DB CON UNA DURACIÓN DE 5 SEGUNDOS.
    private fun splash() = CoroutineScope(Dispatchers.Main).launch(job + errorHandler) {
        delay(5000)
        startActivity(Intent().setClass(this@SplashActivity, MainActivity::class.java))
        finish()
    }

}