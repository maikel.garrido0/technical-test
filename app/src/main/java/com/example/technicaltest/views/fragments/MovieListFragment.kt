package com.example.technicaltest.views.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.technicaltest.adapters.AdapterMovie
import com.example.technicaltest.dataSource.RepoImpl
import com.example.technicaltest.dataSource.VMFactory
import com.example.technicaltest.dataSource.dataSource
import com.example.technicaltest.database.AppDataBase
import com.example.technicaltest.databinding.FragmentMovieListBinding
import com.example.technicaltest.models.Movie
import com.example.technicaltest.services.RetrofitClient
import com.example.technicaltest.utils.OnClickListener
import com.example.technicaltest.utils.message
import com.example.technicaltest.viewmodel.MainViewModel
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import retrofit2.awaitResponse

import android.net.ConnectivityManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.example.technicaltest.R
import com.example.technicaltest.utils.dialogFragments.ErrorConexion
import com.example.technicaltest.utils.dialogFragments.Loading


class MovieListFragment : Fragment(), OnClickListener {

    private var _binding: FragmentMovieListBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    val viewModel by activityViewModels<MainViewModel> { VMFactory(RepoImpl(dataSource(AppDataBase.getDatabase(requireContext().applicationContext)))) }

    lateinit var api: RetrofitClient
    lateinit var dialog: Loading
    private val job = Job()
    private var mFirestore = FirebaseFirestore.getInstance()

    val errorHandler = CoroutineExceptionHandler { _, exception ->
        dialog.dismiss()
        requireContext().message("¡Ocurrio un fallo!")
    }

    private val adapterCom: AdapterMovie by lazy { AdapterMovie(arrayListOf(), this) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        api = RetrofitClient
        dialog = Loading()
        dialog.show(requireActivity().supportFragmentManager, "")
        setupRecyclerview()
        getMovie()
    }

    private fun getMovie() {
        isDeviceOnline {
            when(it) {
                true -> { getPopularMovie() }
                else -> { checkMovieROOM() }
            }
        }
    }

    private fun getPopularMovie() = CoroutineScope(job + Main).launch(errorHandler) {
        val result = api.service.getPopularMovie().awaitResponse()
        if (result.isSuccessful && result.body() != null) {
            val response = result.body()!!
            withContext(Main) {
                Log.e("TAG", "getMovie: ${response.results}")
                response.results?.forEach { movie ->
                    viewModel.insert_Movie(movie)
                    //writeFirebase(movie)
                }
                adapterCom.setMovie(response.results)
                dialog.dismiss()
            }
        } else {
            withContext(Main) {
                Log.e("LOG", "response FALLO")
                dialog.dismiss()
            }
        }
    }

    private fun checkMovieROOM() {
        dialog.dismiss()
        dialog()
        viewModel.getMovieSizeLiveData()?.observe(viewLifecycleOwner,  { cantidad ->
            if (cantidad != null) {
                when(cantidad) {
                    0 -> { requireContext().message("No contiene películas en base de datos...") }
                    else -> {
                        requireContext().message("Se le han cargado las últimas películas guardadas en base de datos...")
                        viewModel.getMovieLiveData().observe(viewLifecycleOwner,  { movie ->
                            adapterCom.setMovie(movie)
                        })
                    }
                }
            }
        })
    }

    private fun setupRecyclerview() {
        binding.rvListPopular.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterCom
        }
    }

    private fun isDeviceOnline(callback: (Boolean) -> Unit) {
        var isConnected = false
        val manager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nInfo = manager.activeNetworkInfo
        if (nInfo != null && nInfo.isConnected) {
            isConnected = true
        }
        callback(isConnected)
    }

    override fun onClick(movie: Movie) {
        Log.e("TAG", "onClick: Movie --> $movie")
        findNavController(this).navigate(R.id.action_movieListFragment_to_movieDetailsFragment)
        viewModel.setMovieSelected(movie)
    }

    fun dialog() {
        val dialog = ErrorConexion()
        dialog.show(requireActivity().supportFragmentManager, "")
    }



}