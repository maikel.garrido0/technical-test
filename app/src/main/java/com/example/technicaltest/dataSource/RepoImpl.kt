package com.example.technicaltest.dataSource

import androidx.lifecycle.LiveData
import com.example.technicaltest.models.Movie

class RepoImpl(private val dataSource: dataSource) : Repository {

    override suspend fun insert_Movie(movie: Movie) {
        dataSource.insert_Movie(movie)
    }

    override fun getMovieLiveData(): LiveData<List<Movie>?> { return dataSource.getMovieLiveData() }

    override fun getMovieSizeLiveData(): LiveData<Int?>? { return dataSource.getMovieSize() }


}