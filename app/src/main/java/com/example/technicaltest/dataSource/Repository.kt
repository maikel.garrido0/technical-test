package com.example.technicaltest.dataSource

import androidx.lifecycle.LiveData
import com.example.technicaltest.models.Movie

interface Repository {
    // INSERTAR PELÍCULA
    suspend fun insert_Movie(movie: Movie)

    //OBTENER PELÍCULAS DE ROOM
    fun getMovieLiveData() : LiveData<List<Movie>?>

    //OBTENER CANTIDAD DE PELÍCULAS DE ROOM
    fun getMovieSizeLiveData(): LiveData<Int?>?

}