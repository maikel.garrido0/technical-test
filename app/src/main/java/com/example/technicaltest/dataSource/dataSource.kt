package com.example.technicaltest.dataSource

import com.example.technicaltest.database.AppDataBase
import com.example.technicaltest.models.Movie
import com.example.technicaltest.providers.ProviderMovie

class dataSource(appDataBase: AppDataBase) {

    var appDataBase = appDataBase

    suspend fun insert_Movie(movie: Movie) = ProviderMovie(appDataBase).insert_Movie(movie)

    fun getMovieLiveData() = ProviderMovie(appDataBase).getMovieLiveData()

    fun getMovieSize() = ProviderMovie(appDataBase).getMovieSizeLiveData()


}