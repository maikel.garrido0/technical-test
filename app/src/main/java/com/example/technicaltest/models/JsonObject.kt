package com.example.technicaltest.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class JsonObject(
    @SerializedName("results")
    var results: List<Movie>?,
) : Parcelable { }

@Parcelize
data class JsonObjectSearch(
    @SerializedName("total_results")
    var total_results: Int?,

    @SerializedName("results")
    var results: List<Movie>?,

): Parcelable { }
