package com.example.technicaltest.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.technicaltest.dataSource.Repository
import com.example.technicaltest.models.Movie
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainViewModel(private val repo: Repository) : ViewModel() {
    // CATCH LOS ERRORES DE LOS COROUTINES

    val job = Job()
    val errorHandler = CoroutineExceptionHandler { _, exception ->
        Log.e("LOG", "ERROR MainViewModel: $exception")
    }
    private val movieSelected = MutableLiveData<Movie>()

    fun insert_Movie(movie: Movie) = viewModelScope.launch(job + errorHandler) {
        repo.insert_Movie(movie)
    }

    fun getMovieLiveData() : LiveData<List<Movie>?> {
        return repo.getMovieLiveData()
    }

    fun getMovieSizeLiveData(): LiveData<Int?>? {
        return repo.getMovieSizeLiveData()
    }

    fun setMovieSelected(movie: Movie) {
        movieSelected.value = movie
    }

    fun getMovieSelected(): MutableLiveData<Movie> {
        return movieSelected
    }


}