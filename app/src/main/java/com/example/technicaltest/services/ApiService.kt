package com.example.technicaltest.services

import com.example.technicaltest.models.JsonObject
import com.example.technicaltest.models.JsonObjectSearch
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/popular?api_key=${RetrofitClient.API_KEY}")
    fun getPopularMovie(): Call<JsonObject>

    @GET("movie/{movie_id}?api_key=${RetrofitClient.API_KEY}")
    fun getMovieByID(@Path("movie_id") movie_id:Int): Call<JsonObject>

    @GET("search/movie?api_key=${RetrofitClient.API_KEY}")
    fun searchMovie(@Query("query") query:String): Call<JsonObject>

}