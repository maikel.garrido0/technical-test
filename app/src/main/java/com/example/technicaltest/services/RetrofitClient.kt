package com.example.technicaltest.services

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    // TODO: 5/1/22 SERVIDOR DE API Y KEY PARA CONSULTAR
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val API_KEY = "aba3634619780b5fc07ac2411f255d01"

    const val LOCATION_SERVICE_ID = 175
    const val INICIAR_SERVICIO = "startLocationService"
    const val DETENER_SERVICIO = "stopLocationService"

    val service: ApiService by lazy {

        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        val client: OkHttpClient = OkHttpClient.Builder()
            .callTimeout(10, TimeUnit.MINUTES)
            .connectTimeout(10, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES)
            .apply {
                this.addInterceptor(interceptor)
            }.build()

        Retrofit.Builder()
            .client(client)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(ApiService::class.java)
    }
}