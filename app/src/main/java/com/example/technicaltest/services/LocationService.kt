package com.example.technicaltest.services

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.example.technicaltest.R
import com.example.technicaltest.utils.message
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.UnsupportedOperationException

class LocationService: Service() {

    private var mFirestore = FirebaseFirestore.getInstance()

    private var locationCallback: LocationCallback? = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val latitude: Double = locationResult.lastLocation.latitude
            val longitude: Double = locationResult.lastLocation.longitude
            writeFirebase(locationResult)
            message("$latitude,$longitude")
            Log.e("TAG", "onLocationResult: $latitude --> $longitude")
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        throw UnsupportedOperationException("No se puede soportar la operación")
    }

    private fun initService() {
        val channelID = "Localización"
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent()
        val pendingIntent = PendingIntent.getActivity(
            applicationContext,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val builder = NotificationCompat.Builder(
            applicationContext,
            channelID
        )
        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.setContentTitle("Servicio de localización...")
        builder.setDefaults(NotificationCompat.DEFAULT_ALL)
        builder.setContentText("Iniciando servicio...")
        builder.setContentIntent(pendingIntent)
        builder.setAutoCancel(false)
        builder.priority = NotificationCompat.PRIORITY_MAX

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null && notificationManager.getNotificationChannel(channelID) == null ) {
                val notificationChannel = NotificationChannel(
                    channelID,
                    "Localización...",
                    NotificationManager.IMPORTANCE_HIGH
                )
                notificationChannel.description = "$locationCallback.lat"
                notificationManager.createNotificationChannel(notificationChannel)
            }
        }

        val locationRequest = LocationRequest()
        locationRequest.interval = 50000
        locationRequest.fastestInterval = 2000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        LocationServices.getFusedLocationProviderClient(this)
            .requestLocationUpdates(locationRequest, locationCallback!!, Looper.getMainLooper())
        startForeground(RetrofitClient.LOCATION_SERVICE_ID, builder.build())
    }

    private fun stopService() {
        LocationServices.getFusedLocationProviderClient(this)
            .removeLocationUpdates(locationCallback!!)
        stopForeground(true)
        stopSelf()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
          val accion = intent.action
            if (accion != null) {
                when(accion) {
                    RetrofitClient.INICIAR_SERVICIO -> { initService() }
                    else -> { stopService() }
                }
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun writeFirebase(locationResult: LocationResult) {
        val location = HashMap<String, Any>()
        location["latitude"] = locationResult.lastLocation.latitude
        location["longitude"] = locationResult.lastLocation.longitude
        location["timetamps"] = FieldValue.serverTimestamp()
        mFirestore.collection("location")
            .add(location)
            .addOnSuccessListener { message("Nueva localización guardada en Firebase...") }
            .addOnFailureListener { e -> Log.w("TAG", "Error", e) }
    }

}