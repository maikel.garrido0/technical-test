package com.example.technicaltest.providers

import androidx.lifecycle.LiveData
import com.example.technicaltest.database.AppDataBase
import com.example.technicaltest.models.Movie

class ProviderMovie(appDataBase: AppDataBase) {

    private var daoDB = appDataBase.DAOMovie()

    suspend fun insert_Movie(movie: Movie): Movie {
        daoDB.insertMovie(movie)
        return movie
    }

    fun getMovieLiveData(): LiveData<List<Movie>?> { return daoDB.getMovieLiveData() }

    fun getMovieSizeLiveData() : LiveData<Int?>? { return daoDB.getMovieSize() }


}