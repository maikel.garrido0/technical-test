package com.example.technicaltest.adapters

import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.technicaltest.databinding.ItemMovieBinding
import com.example.technicaltest.models.Movie
import com.example.technicaltest.utils.OnClickListener

class AdapterMovie(
    private var listmovie: List<Movie>,
    private val listener: OnClickListener
): RecyclerView.Adapter<AdapterMovie.MovieViewHolder>() {

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = ItemMovieBinding.inflate(LayoutInflater.from(parent.context) , parent,false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = listmovie[position]
        val URL_IMAGE = "https://image.tmdb.org/t/p/w500/"
        holder.viewBinding.idNameMovie.text = item.title
        holder.viewBinding.idDescriptionMovie.text = item.overview
        holder.viewBinding.idDateMovie.text = item.release_date
        Glide.with(holder.viewBinding.idImage).load(URL_IMAGE+item.poster_path).into(holder.viewBinding.idImage)
        holder.listener(item)
    }

    override fun getItemCount(): Int { return listmovie.size }


    inner class MovieViewHolder(var viewBinding: ItemMovieBinding) : RecyclerView.ViewHolder(viewBinding.root) {
        fun listener(movie: Movie) {
            with(viewBinding.root) {
                setOnClickListener {
                    val now = System.currentTimeMillis()
                    when (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                        true -> {
                            return@setOnClickListener
                        }
                        else -> {
                            mLastClickTime = now
                            listener.onClick(movie)
                        }
                    }
                }
            }
        }
    }

    internal fun setMovie(rowItems: List<Movie>?) {
        if (rowItems != null) { listmovie = rowItems }
        notifyDataSetChanged()
    }


}