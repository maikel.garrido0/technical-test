package com.example.technicaltest.utils

import com.example.technicaltest.models.Movie

interface OnClickListener {
    fun onClick(movie: Movie)
}