package com.example.technicaltest.utils

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.technicaltest.R
import com.example.technicaltest.database.AppDataBase
import java.text.DateFormat
import java.util.*

fun createTables(context: Context){
    val room = AppDataBase.getDatabase(context)
    // ROOM CREACION DE TABLAS
    room.DAOMovie()

}

/* MENSAJE CON UN TOAST */
fun Context.message(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

/*VERIFICAR SI EL DISPOSITIVO TIENE O NO INTERNET*/
@SuppressLint("MissingPermission")
fun Context.isDeviceOnline(): Boolean {
    var isConnected = false
    val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val nInfo = manager.activeNetworkInfo
    if (nInfo != null && nInfo.isConnected) {
        isConnected = true
    }
    return isConnected
}